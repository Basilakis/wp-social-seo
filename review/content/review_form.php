<?php 

$review_data = (array)get_option( 'wp_social_seo_review_tab' );

$dn = new wpsocial_DotNotation( $review_data ); 

?>


				<div class="metabox-holder content-container"> 
					<div class="postbox-container wps-postbox-container">    
						<div class="meta-box-sortables ui-sortable">        
							<div class="postbox">
								<div class="handlediv" title="Click to toggle"><br/></div>
								<h3 class="hndle"><span>General Settings</span></h3>
								<div class="inside">

									<?php global $wpdb;
									    
	                                    ?>
	                                    <div class='review-setup'>
	                                    	<form action='' method='post'>
		                                    	<table>
		                                    		<tr>
		                                    			<td><label>Full Name:</label></td>
		                                    			<td>
		                                    				<input type='text' name='review[full_name]' value="<?php echo $dn->get( 'full_name' ); ?>" />
		                                    			</td>
			                                    	</tr>
			                                    	<tr>
			                                    		<td><label>Company Name:</label></td>
			                                    		<td>
			                                    			<input type='text' name='review[company_name]' value="<?php echo $dn->get( 'company_name' ); ?>" />
			                                    		</td>
			                                    	</tr>
			                                    	<tr>
			                                    	<td><label>Header Logo URL:</label></td>
			                                    		<td>
			                                    			<input type='text' name='review[logo_header]' value="<?php echo $dn->get( 'logo_header' ); ?>" />
			                                    		</td>
			                                    	</tr>
			                                    	<tr>
			                                    		<td><label>Subject:</label></td>
			                                    		<td>
			                                    			<textarea name='review[subject]' rows='2' cols='80'>
			                                    				<?php echo $dn->get( 'subject' ); ?>
			                                    			</textarea>
			                                			</td>
			                            			</tr>
			                            			<tr>
			                            				<td><label>Message:</label></td>
			                            				<td>
			                            					<textarea name='review[message]' rows='10' cols='80'>
			                            						<?php echo $dn->get( 'message' ); ?>
			                            					</textarea>
			                        					</td>
			                    					</tr>
			                    					<tr>
			                                    		<td><label>Review us at:</label></td>
			                                    		<td>
			                                    			<input type='text' name='review[review_link_1]' value="<?php echo $dn->get( 'review_link_1' ); ?>" />
			                                    			<input type='text' name='review[review_link_2]' value="<?php echo $dn->get( 'review_link_2' ); ?> " />
			                                    			<input type='text' name='review[review_link_3]' value="<?php echo $dn->get( 'review_link_3' ); ?>" />
			                                    		</td>
			                                    	</tr>
								                    <tr>
			                                    		<td><label>Footer Logo URL:</label></td>
			                                    		<td>
			                                    			<input type='text' name='review[logo_footer]' value="<?php echo $dn->get( 'logo_footer' ); ?>" />
			                                    		</td>
			                                    	</tr>
								                    <tr>
			                                    		<td><label>Phone Number:</label></td>
			                                    		<td>
			                                    			<input type='text' name='review[phone_num]' value="<?php echo $dn->get( 'phone_num' ); ?>" />
			                                    		</td>
			                                    	</tr>
			                                    	 <tr>
			                                    		<td><label>Email:</label></td>
			                                    		<td>
			                                    			<input type='text' name='review[Email]' value="<?php echo $dn->get( 'Email' ); ?>" />
			                                    		</td>
			                                    	</tr>
								                    <tr>
			                            				<td><label>Footer Text Top:</label></td>
			                            				<td>
			                            					<textarea name='review[footer_text_top]' rows='8' cols='80'>
			                            						<?php echo $dn->get( 'footer_text_top' ); ?>
			                            					</textarea>
			                        					</td>
			                    					</tr>
								                    <tr>
			                            				<td><label>Footer Text:</label></td>
			                            				<td>
			                            					<textarea name='review[footertext]' rows='8' cols='80'>
			                            						<?php echo $dn->get( 'footertext' ); ?>
			                            					</textarea>
			                        					</td>
			                    					</tr>
			                    					  <tr>
								                    	<td>
								                    		<input type='hidden' name='review_message' value='1'><input type='submit' value='Submit'>
								                    	</td>
								                    </tr>
		                						</table>
	            							</form>
	        							</div>
								 </div>
							</div>
						</div>
					</div>
				</div>

	

	

