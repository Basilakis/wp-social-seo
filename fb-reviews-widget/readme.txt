=== Facebook Reviews ===
Contributors: richplugins
Donate link: https://richplugins.com/
Tags: facebook, facebook reviews, facebook business reviews, facebook page review, Facebook Page, facebook review, facebook review widget, business review, reviews, facebook reviews widget, comment, comments, sidebar, widget, richplugins, widgetpack
Requires at least: 2.8
Tested up to: 4.7
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Facebook Reviews Widget show Facebook Page reviews on your WordPress website to increase user confidence and SEO.

== Description ==

> > Why you better use <a href="https://richplugins.com/facebook-reviews-pro-wordpress-plugin">Facebook Reviews Pro</a> plugin

This plugin display Facebook Reviews on your websites in sidebar widget.

Keep in mind that plugin requests Facebook permission <a href="https://developers.facebook.com/docs/facebook-login/permissions#reference-manage_pages" target="_blank">manage_pages</a> to read your page reviews and show it in the widget.

[Online demo](http://demo.richplugins.com/)

= Plugin Features =

* Free!
* SEO
* Display all Facebook business reviews per location
* Shows real reviews from Facebook users to increase user confidence
* Easy get of Facebook pages and instantly show reviews

= Get More Features with Facebook Reviews Pro! =

[Upgrade to Facebook Reviews Pro](https://richplugins.com/facebook-reviews-pro-wordpress-plugin)

* Supports Google Rich Snippets (schema.org)
* Powerful <b>Shortcode Builder</b>
* Trust Facebook badge (fixed or inner)
* Trim long reviews and add "read more" link
* Minimum review rating filter
* Pagination
* Nofollow, target="_blank" links
* Priority support

== Installation ==

1. Unpack archive to this archive to the 'wp-content/plugins/' directory inside of WordPress
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Facebook Reviews widget
2. Facebook Reviews sidebar

== Changelog ==

= 1.1 =
* Bugfix: time-ago on English by default, update readme

== Support ==

* Email support support@richplugins.com
* Live support https://richplugins.com/forum
