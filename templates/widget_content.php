<?php

        wp_enqueue_style('carouselcss', plugins_url('/css/jquery.bxslider.css', __FILE__));
        wp_enqueue_script('jquery');
        //wp_enqueue_script('custom_carousel', plugins_url('js/jquery.bxslider.js', __FILE__));
   
        wp_enqueue_script('custom_carousel', plugins_url('/../fb-reviews-widget/static/js/jquery.bxslider.js', __FILE__));
        require_once(plugin_dir_path(__FILE__) . '/../fb-reviews-widget/fbrev-reviews-helper.php');
        require_once(plugin_dir_path(__FILE__) . '/../widget-google-reviews/grw-reviews-helper.php');
        wp_enqueue_style('fbrev_widget_icon', plugins_url('/fonts/font-awesome.css', __FILE__));

global $wpdb;
  
    if($rotate_random) {
        shuffle( $res ); 
    }

    
    if($rotate_number) {
            //print_r($rotate_number); exit('asd');
        $res = array_slice($res, 0, $rotate_number);  
        //print_r( $res ); exit('sdfsdf');

    }
    //print_r($res ); exit();
    ?>
    <div class="bxslider-reviews-fb">

  <?php  foreach ($res  as $re) { 
      

  $ndn = new wpsocial_DotNotation( $re );
 
    $page_image = $ndn->get( 'page_image' );
    
    $page_name = $ndn->get( 'page_name' );
 
    $img = $ndn->get( 'image' );
    $page_link = $ndn->get( 'page_link' );
    $rev_type = $ndn->get( 'type' );
    if($rev_type == 'google') : $stars='grw_stars'; else: $stars ='fbrev_stars'; endif;
    if($rev_type == 'fb') : $icon='fa-facebook-square'; elseif($rev_type == 'google'): $icon ='fa-google';  else: $icon = 'fa-shield';endif;
    $name = $ndn->get('name');
    
    $cover_photo = fbrev_get_cover_photo( $page_id, $page_access_token );
    
    
    
    ?>



        <div class="wp-facebook-rew wpsocial-<?php echo esc_attr( $rev_type ); ?> <?php if( $dark_color){?> wp_dark <?php }?>">
            <div class="wp-facebook-review">
                <div class="wp-facebook-left">
                    <img src="<?php echo esc_url($img); ?>">

                </div>

                <div class="wp-facebook-right clearfix">
                    <?php //fbrev_anchor('https://www.facebook.com/app_scoped_user_id/' . $re->author_id, 'wp-facebook-name', $re->reviewer_name, true, true); ?>
                      <span class=""><?php echo $name; ?></span>
                      <div class="icon-fb"><a href="<?php echo esc_url($page_link); ?>" target="_blank"><i class="fa <?php echo $icon; ?>"></i></a></div>

                    <div class="wp-facebook-feedback">
                        <span class="wp-facebook-stars"><?php echo $stars( $ndn->get( 'rating' ) ); ?></span>
                      
                     
                    </div>
                    
                </div>
              
            </div>
           
             <span class="wp-facebook-text"><?php echo $ndn->get( 'text' ) ; ?></span>
            <?php   if($rev_type == 'fb'){ ?>
                <div class="image_cover">
                    <img src="<?php echo esc_url($cover_photo); ?>">
                    <div class="cover-upper">
                        <a href="<?php echo esc_url($page_link); ?>" target="_blank"><img src="<?php echo esc_url($page_image); ?>"></a>
                        <div class="page_name">
                            <a href="<?php echo esc_url($page_link); ?>" target="_blank"><?php echo ($page_name); ?></a>
                        </div>    
                    </div>
                </div>
            <?php }?>    

        </div>
<?php } 




?>
<style>
    .image-review {
        min-height: 71px;
    }
    .image-fb img {
    background: #eee none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 50%;
    height: 67px;
    margin-right: 10px;
    padding: 5px;
    width: 67px;
}
.rating-review > a {
    display: block;
}
.wp-facebook-text.name {
    color: #222;
    font-family: inherit;
    font-weight: 500;
    margin-bottom: 6px;
}
.wp-facebook-text {
    display: block;
}
.wp-facebook-right.clearfix > span {
    display: inline-block;
    width: 50%;
}
.wpsocial-custom .image-fb {
    display: none;
}
.image-fb {
    float: left;
}
.wp-facebook-rew {
    border: 1px solid #eee;
    min-height: 300px;
    padding: 10px;
}
.rating-review > a {
    color: #222 !important;
}
.wp-facebook-left {
    float: left;
    margin-right: 10px;
}
.wp-facebook-left {
    float: left;
    margin-right: 10px;
}
.wp_dark .wp-facebook-right.clearfix > span, .wp_dark .wp-facebook-text, .wp_dark .image > a {
    color: #fff;
}
.wp-facebook-rew.wpsocial-fb {
    position: relative;
}
.widget.wpsocial_review_Widget .bx-wrapper {
    overflow-x: hidden;
}
.fa-facebook-square:before {
  content: "\f230";
}
.fa-facebook-square {
  font-family: 'fontawesome';
}
.icon-fb > a {
    font-size: 20px;
}
.wpsocial-fb .icon-fb > a, .wpsocial-custom .icon-fb > a {
    color: rgb(60, 91, 155) !important;
}
.wpsocial-google .icon-fb > a {
    color: orange !important;
}
#sidebar .widget, #sidebar .widget a {
    color: #888;
}
.icon-fb {
    float: right;
}
.wp-facebook-left > img {
    border-radius: 50%;
    height: 70px;
    width: 70px;
}
.wp-facebook-right.clearfix > span {
    color: #222;
    font-size: 14px !important;
    font-weight: bold;
    letter-spacing: 0.2px;
    line-height: 27px !important;
}
.wp-facebook-text {
    color: #222;
    font-family: "Open Sans",Helvetica,Arial,sans-serif;
    margin-bottom: 11px;
    margin-top: 11px;
}
.image_cover > img {
    height: 100%;
    width: 100%;
}
.image_cover {
    position: relative;
}
.cover-upper {
    bottom: 23px;
    display: inline-flex;
    left: 10px;
    position: absolute;
}
.cover-upper > a {
    color: #fff !important;
    font-size: 16px !important;
    font-weight: bold;
}
.page_name > a {
    color: #fff;
    font-size: 20px;
}
.cover-upper img {
    border: 5px solid;
    border-radius: 5px;
    margin-right: 20px;
}

</style>


</div> 

<script>jQuery(document).ready(function () {           
        jQuery('.bxslider-reviews-fb').bxSlider({
        pager :false,
        auto:true,
        mode:'horizontal',
        speed: 5000,
        pause: 20000,
        controls:false,
        autoHover:true
        }); 
   
        });
</script>  
        